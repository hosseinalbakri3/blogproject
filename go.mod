module gitlab.com/hosseinalbakri3/blogproject

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.14
	github.com/shiyanhui/hero v0.0.2
	github.com/yaa110/go-persian-calendar v0.5.0
	golang.org/x/crypto v0.0.0-20200707235045-ab33eee955e0
)
