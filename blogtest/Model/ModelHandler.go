package Model

import "github.com/jinzhu/gorm"
import _ "github.com/jinzhu/gorm/dialects/mysql"

type MHandler struct {
	db *gorm.DB
}

func NewModelHandler(connStr string) (*MHandler, error) {
	db, err := gorm.Open("mysql", connStr)
	if err != nil {
		return nil, err
	}

	db.Table("articles").AutoMigrate(&Article{})
	db.Table("categories").AutoMigrate(&Category{})
	db.Table("users").AutoMigrate(&User{})
	db.Table("tokens").AutoMigrate(&AuthToken{})

	return &MHandler{db: db}, nil
}
