package Model

type AuthToken struct {
	ID     uint   `gorm:"primary_key"`
	Token  string `gorm:"type:varchar(1024);not null"`
	UserId int
}

func (handler MHandler) CreateNewToken(userId int, token string) error {
	authToken := AuthToken{
		Token:  token,
		UserId: userId,
	}
	if result := handler.db.Table("tokens").Create(&authToken); result.Error != nil {
		return result.Error
	}
	return nil
}

func (handler MHandler) TokenIsExist(token string) bool {
	var authToken AuthToken
	if result := handler.db.Table("tokens").Where("token = ?", token).
		First(&authToken); result.Error != nil {
		return false
	}
	return true
}
