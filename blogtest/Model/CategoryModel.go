package Model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"time"
)

type Category struct {
	ID        int `gorm:"primary_key"`
	Name      string
	EngName   string
	CreatedAt time.Time `json:"createdAt" gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `json:"updatedAt" gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

func (c Category) Validate() error {
	errors := validation.ValidateStruct(&c,
		validation.Field(&c.Name , validation.Required.Error("نام نمیتواند خالی باشد"),
			validation.NotNil.Error("نام نمیتواند نیل باشد")),
		validation.Field(&c.EngName , validation.Required.Error("نام انگلیسی نمیتواند خالی باشد") ,
			validation.NotNil.Error("نام انگلیسی نمیتواند نیل باشد")),
	)
	return errors
}

func (handler MHandler) CreateCategory(category Category) error {
	result := handler.db.Table("categories").Create(&category)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func (handler MHandler) DeleteCategory(id int) error {
	if result := handler.db.Table("categories").Where("id = ? " , id).Delete(&Category{}) ; result.Error != nil {
		return result.Error
	}
	return nil
}

func (handler MHandler) GetCategoryById(id int) (Category, error) {
	var category Category
	if result := handler.db.Table("categories").First(&category, id); result.Error != nil {
		return category, result.Error
	}

	return category, nil
}

func (handler MHandler) GetAllCategories() ([]Category, error) {
	var allCat []Category
	if result := handler.db.Table("categories").Find(&allCat); result.Error != nil {
		return allCat, result.Error
	}

	return allCat, nil
}
