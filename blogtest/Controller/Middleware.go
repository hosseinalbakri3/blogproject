package Controller

import "net/http"

func (c Controller) AdminAuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter , r *http.Request) {
		err := IsLogIn(c, r)
		if err != nil {
			http.Error(w,err.Error(),http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w,r)
	})
}
