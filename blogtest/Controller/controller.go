package Controller

import (
	Model "gitlab.com/hosseinalbakri3/blogproject/blogtest/Model"
	"gitlab.com/hosseinalbakri3/blogproject/blogtest/Static/gotemplate"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"net/http"
	"strconv"
)

type Controller struct {
	handler *Model.MHandler
}

func NewController(connStr string) (*Controller, error) {
	mHandler, err := Model.NewModelHandler(connStr)
	if err != nil {
		return nil, err
	}

	return &Controller{handler: mHandler}, err
}

func (c Controller) GetIndexPage(w http.ResponseWriter, r *http.Request) {
	var arts []Model.Article
	var cats []Model.Category
	cats, err := c.handler.GetAllCategories()
	if err != nil {
		w.WriteHeader(500)
		gotemplate.IndexPage(cats, arts, w)
		return
	}

	arts, err = c.handler.GetALLArticle()
	if gorm.IsRecordNotFoundError(err) || len(arts) == 0 {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintln(w, "داده ای وجود ندارد")
		return
	} else if err != nil {
		w.WriteHeader(500)
		fmt.Fprintln(w, "خطا داخلی")
		return
	}
	gotemplate.IndexPage(cats, arts, w)
}

func (c Controller) GetSinglePage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postId := vars["postId"]
	id, err := strconv.Atoi(postId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "درخواست داده شده اشتباه است")
		return
	}
	article, err := c.handler.GetArticleById(id)
	if gorm.IsRecordNotFoundError(err) {
		fmt.Fprintln(w, "داده ای موجود نیست")
		return
	}
	gotemplate.SinglePage(article, w)
}

func (c Controller) GetIndexPageByCategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	catId := vars["catId"]
	cid, err := strconv.Atoi(catId)

	var arts []Model.Article
	var cats []Model.Category

	cats, err = c.handler.GetAllCategories()
	if err != nil {
		fmt.Fprintln(w, err)
		w.WriteHeader(500)
		return
	}

	arts, err = c.handler.GetArticleByCategory(cid)
	if gorm.IsRecordNotFoundError(err) || len(arts) == 0 {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintln(w, "داده ای وجود ندارد")
		return
	} else if err != nil {
		w.WriteHeader(500)
		fmt.Fprintln(w, "خطا داخلی")
		return
	}

	gotemplate.IndexPage(cats, arts, w)
}

func (c Controller) GroupBy(w http.ResponseWriter, r *http.Request) {
	c.handler.GroupBy()
}




