package Controller

import (
	"gitlab.com/hosseinalbakri3/blogproject/blogtest/Model"
	"gitlab.com/hosseinalbakri3/blogproject/blogtest/Static/gotemplate"
	"gitlab.com/hosseinalbakri3/blogproject/blogtest/Util"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const JwtKey = "thH123isisFjFFwtkPeFyM"

func (c Controller) SignIn(w http.ResponseWriter, r *http.Request) {
	var rUser Model.User
	userName := r.FormValue("username")
	rowPass := r.FormValue("password")
	hashPass, err := bcrypt.GenerateFromPassword([]byte(rowPass), bcrypt.DefaultCost)
	if err != nil {
		http.Error(w, "error 500 happened 1", 500)
		return
	}
	rUser.UserName = userName
	rUser.HashPassword = string(hashPass)
	dbUser, err := c.handler.CreateUser(rUser)
	if err != nil {
		http.Error(w, "error 500 happened 2", 500)
		return
	}

	extTime := time.Now().Add(364 * 24 * time.Hour)
	tokenString, err := GenerateNewToken(userName, extTime)
	if err != nil {
		fmt.Fprintln(w, err)
		http.Error(w, "error 500 happened 3", 500)
		return
	}

	err = c.handler.CreateNewToken(int(dbUser.ID), tokenString)
	if err != nil {
		fmt.Fprintln(w, err)
		http.Error(w, "error 500 happened 3", 500)
		return
	}

	cookie := http.Cookie{
		Name:    "JwtToken",
		Value:   tokenString,
		Expires: extTime,
	}

	http.SetCookie(w, &cookie)

	fmt.Fprintln(w, "done")
}

func (c Controller) LoginGet(w http.ResponseWriter, r *http.Request) {

	gotemplate.LoginPage(nil, w)
}

func (c Controller) LoginPost(w http.ResponseWriter, r *http.Request) {

	var msg []string
	password := strings.TrimSpace(r.FormValue("password"))
	username := strings.TrimSpace(r.FormValue("username"))

	err := util.ValidatePassword(password)
	if err != nil {
		msg = append(msg, err.Error())
	}

	if username == "" {
		msg = append(msg, "نام کاربری نمیتواند خالی باشد")
	}
	if len(msg) > 0 {
		gotemplate.LoginPage(msg, w)
		return
	}

	user, err := c.handler.GetUserByUsername(username)
	if err != nil {
		msg = append(msg, "کاربری با این نام کاربری وجود ندارد")
		gotemplate.LoginPage(msg, w)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.HashPassword), []byte(password))
	if err != nil {
		msg = append(msg, "رمز عبور اشتباه است")
		gotemplate.LoginPage(msg, w)
		return
	}

	extTime := time.Now().Add(364 * 24 * time.Hour)
	tokenString, err := GenerateNewToken(username, extTime)

	err = c.handler.CreateNewToken(int(user.ID), tokenString)
	if err != nil {
		msg = append(msg, "مشکلی در سرور بوجود آمده میبخشید :) ")
		gotemplate.LoginPage(msg, w)
		return
	}

	cookie := http.Cookie{
		Name:    "JwtToken",
		Value:   tokenString,
		Expires: extTime,
	}
	http.SetCookie(w, &cookie)

	fmt.Fprintln(w, "you are logged in ")
}

func (c Controller) CheckIsAuth(w http.ResponseWriter, r *http.Request) {
	co, err := r.Cookie("JwtToken")
	if err != nil {
		http.Error(w, "not logged in", http.StatusUnauthorized)
		return
	}

	tokenString := co.Value

	token, err := jwt.ParseWithClaims(tokenString, &util.Claim{}, func(token *jwt.Token) (i interface{}, e error) {
		return []byte(JwtKey), nil
	})
	if !token.Valid {
		http.Error(w, "not logged in", 402)
		return
	}
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			http.Error(w, "خطا در توکن ارسال شده", http.StatusUnauthorized)
			return
		}
		http.Error(w, "خطا در توکن ارسال شده", http.StatusBadRequest)
		return
	}

	ok := c.handler.TokenIsExist(tokenString)
	if !ok {
		http.Error(w, "خطا در توکن ارسال شده d", http.StatusBadRequest)
		return
	}
}

func (c Controller) CreateGroupGet(w http.ResponseWriter, r *http.Request) {
	gotemplate.AdminCreateCategoryPage(nil, w)
}

func (c Controller) CreateGroupPost(w http.ResponseWriter, r *http.Request) {
	var msg []string

	Name := r.FormValue("name")
	enName := r.FormValue("enName")

	category := Model.Category{
		Name:    Name,
		EngName: enName,
	}

	err := category.Validate()
	if err != nil {
		msg = append(msg, err.Error())
		gotemplate.AdminCreateCategoryPage(msg, w)
		return
	}

	err = c.handler.CreateCategory(category)
	if err != nil {
		msg = append(msg, " اوپس یکی پاش رفته رو سیم میبخشد :) ")
		gotemplate.AdminCreateCategoryPage(msg, w)
		return
	}

	msg = append(msg, "عنوان با موفقیت ساخته شد")
	gotemplate.AdminCreateCategoryPage(msg, w)
}

func (c Controller) CategoryList(w http.ResponseWriter, r *http.Request) {
	categories, err := c.handler.GetAllCategories()
	if err != nil {
		http.Error(w, "اوپس سرور خراب شده میبخشید :)", http.StatusInternalServerError)
		return
	}

	gotemplate.AdminCategoryListPage(categories, w)
}

func (c Controller) DeleteCategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idVar := vars["catId"]

	id, err := strconv.Atoi(idVar)
	if err != nil {
		http.Error(w, "اوپس درخواستت خرابه داداچ :)", http.StatusBadRequest)
		return
	}

	err = c.handler.DeleteCategory(id)
	if err != nil {
		http.Error(w, "اوپس سرور مشکل پیدا کرده :)", http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/admin/categorylist", http.StatusSeeOther)
}

func (c Controller) EditCategoryGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	varid := vars["catId"]

	id, err := strconv.Atoi(varid)
	if err != nil {
		http.Error(w, "دادچ درخواستت اشتباهه", http.StatusBadRequest)
		return
	}
	category, err := c.handler.GetCategoryById(id)
	if err != nil {
		gotemplate.AdminEditCategoryPage([]string{"میبخشید یه مشکلی برا سرورمون درس شده یکم دیگه دوباره بیاین",}, category, w)
		return
	}
	gotemplate.AdminEditCategoryPage(nil, category, w)
}

func (c Controller) EditCategoryPost(w http.ResponseWriter, r *http.Request) {

}

func (c Controller) ArticleList(w http.ResponseWriter, r *http.Request) {
	articles, err := c.handler.GetALLArticle()
	if err != nil {
		http.Error(w, "internal error ", http.StatusInternalServerError)
		return
	}
	gotemplate.AdminPostListPage(articles, w)
}

func (c Controller) CreateArticleGet(w http.ResponseWriter, r *http.Request) {
	cats, err := c.handler.GetAllCategories()
	if err != nil {
		http.Error(w, "internal error happened ", http.StatusInternalServerError)
		return
	}
	gotemplate.AdminCreatePostPage(nil, cats, w)
}

func (c Controller) CreateArticlePost(w http.ResponseWriter, r *http.Request) {
	article := Model.Article{}
	article.Title = r.FormValue("title")

	sCatId := r.FormValue("CatID")
	catId, err := strconv.Atoi(sCatId)
	if err != nil {
		http.Error(w, "bad request ", http.StatusBadRequest)
		return
	}
	article.CatId = catId

	article.ShortDesc = r.FormValue("ShortDesc")
	article.LongDesc = r.FormValue("LongDesc")

	var picName string
	f, fh, err := r.FormFile("myFile")

	fmt.Println("----------------------")
	//defer f.Close()
	if f != nil {
		picName, err = util.GenerateFileName(f, fh.Filename)
		if err != nil {
			http.Error(w, " internal error uploading picture", http.StatusInternalServerError)
			return
		}
		article.Image = picName
	}
	// validate the article
	err, errTxt := article.Validation()
	if err != nil {
		fmt.Fprintln(w, errTxt)
		return
	}
	//upload image
	if f != nil {
		err = util.UploadFile(f, "Static/img", picName)
		if err != nil {
			fmt.Println(err)
			http.Error(w, "there is problem on file", http.StatusInternalServerError)
			return
		}
	}

	err = c.handler.CreateArticle(&article)
	if err != nil {
		http.Error(w, "internal error happened", http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/admin/articles", http.StatusSeeOther)
}

func (c Controller) DeleteArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	aId := vars["artID"]
	id, err := strconv.Atoi(aId)
	if err != nil {
		fmt.Fprintln(w, id, "ریکوست خرابه", aId)
		http.Error(w, "bad request ", http.StatusBadRequest)
		return
	}
	err = c.handler.DeleteArticle(id)
	if err != nil {
		fmt.Fprintln(w, " ریدیم عمو")
		http.Error(w, "internal Error", http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/admin/articles", http.StatusSeeOther)
}

func (c Controller) EditArticleGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	aId := vars["artID"]
	id, err := strconv.Atoi(aId)
	if err != nil {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}
	cats, err := c.handler.GetAllCategories()
	if err != nil {
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	article, err := c.handler.GetArticleById(id)
	if err != nil {
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	gotemplate.AdminEditPostHandler(nil, cats, article, w)
}

func (c Controller) EditArticlePost(w http.ResponseWriter, r *http.Request) {
	article := Model.Article{}

	vars := mux.Vars(r)
	aID := vars["artID"]
	id, err := strconv.Atoi(aID)
	if err != nil {
		http.Error(w, "internal error happened 1", http.StatusInternalServerError)
		return
	}
	article.ID = id
	article.Title = r.FormValue("title")
	article.Image = r.FormValue("oldImageName")
	article.LongDesc = r.FormValue("LongDesc")
	article.ShortDesc = r.FormValue("ShortDesc")
	cId := r.FormValue("CatId")
	id, err = strconv.Atoi(cId)
	if err != nil {
		http.Error(w, "internal error happened 2", http.StatusInternalServerError)
		return
	}
	article.CatId = id

	// validation
	err, msg := article.Validation()
	if err != nil {
		cats, err := c.handler.GetAllCategories()
		if err != nil {
			http.Error(w, "internal error 3", http.StatusInternalServerError)
			return
		}

		article, err := c.handler.GetArticleById(article.ID)
		if err != nil {
			http.Error(w, "internal error 4", http.StatusInternalServerError)
			return
		}

		gotemplate.AdminEditPostHandler(msg, cats, article, w)
		return
	}

	f, mf, err := r.FormFile("myFile")
	if f != nil {
		imgName, err := util.GenerateFileName(f, mf.Filename)
		if err != nil {
			http.Error(w, "internal error happened 5", http.StatusInternalServerError)
			return
		}
		if imgName != article.Image {
			fmt.Println("change")
			err = util.UploadFile(f, "Static/img", imgName)
			if err != nil {
				if err != nil {
					http.Error(w, "internal error happened 6", http.StatusInternalServerError)
					return
				}
			}
		}
		article.Image = imgName
	}

	err = c.handler.UpdateArticle(article)
	if err != nil {
		http.Error(w, "internal error happened 7", http.StatusInternalServerError)
		return
	}

	http.Redirect(w,r,"/admin/articles",http.StatusSeeOther)

}

//here i change something
